# Projet Isola

Aller sur la page wikipedia du jeu <a href="https://fr.wikipedia.org/wiki/Isola_%28jeu%29" target="_blank">Isola</a>.

Vous devez tout simplement programmer ce jeu en utilisant les [images données](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/archive/master/mooc-2-_-pratique-master.zip?path=2_Mise-en-Pratique-Professionnelle/2.3_Accompagner/2.3.2_Exemples/Divers-projets/Isola).
