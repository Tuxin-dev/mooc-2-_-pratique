# MOOC 2 - Pratique


## Module Introduction

1. Présenter ce mooc, son but, la démarche
2. Vidéos interviews d'expériences de prof.e.s : des choses qui ont bien marché, des difficultés (d'ordre didactique ou pédagogique, d'ordre technique...), des séances _catastrophes_ et ce que ça a enseigné (à l'enseignant.e) etc.
3. _what else?_

## Module _Par thème_

Ici on décline quelques sections suivants les thèmes NSI (ceux utilisés dans le MOOC 1 par exemple) et pour chaque section des activités _à tester_ ? 

### Données

Le niveau NSI 1re concerne les fichiers textes et les données en tables ; le niveau Terminale propose des activités sur les bases de données.

**Niveau 1re : fichiers textes, données en tables**

- Introduction : le programme officiel [00_DonneesEnTable_intro.md](02_Module_Thematique/01_Donnees/00_DonneesEnTable_intro.md)
- Une première activité simple ; un TP pour manipuler les fichiers textes simples et petits : [01_DonneesEnTable_activite01.md](02_Module_Thematique/01_Donnees/01_DonneesEnTable_activite01.md) Ressources supplémentaires : un exemple de fiche TP [02_DonneesEnTable_FicheEleve01.ipynb](02_Module_Thematique/01_Donnees/02_DonneesEnTable_FicheEleve01.ipynb) et la fiche explicative [02_DonneesEnTable_FicheProf01.md](02_Module_Thematique/01_Donnees/02_DonneesEnTable_FicheProf01.md) sur les intentions de cette activité
- Une seconde activité pour manipuler de vraies données en tables, d'une taille suffisante pour rendre l'utilisation via un tableur fastidieuse : [01_DonneesEnTable_activite02.md](02_Module_Thematique/01_Donnees/01_DonneesEnTable_activite02.md)

**Niveau Terminale : bases de données, langage SQL**

- Introduction [03_SQL_intro.md](02_Module_Thematique/01_Donnees/03_SQL_intro.md)
- Préparer un ensemble de séances pour SQL [04_SQL_activite01.md](02_Module_Thematique/01_Donnees/04_SQL_activite01.md)
- Préparer une évaluation [04_SQL_activite02.md](02_Module_Thematique/01_Donnees/04_SQL_activite02.md)
- Préparer une remédiation [04_SQL_activite03.md](02_Module_Thematique/01_Donnees/04_SQL_activite03.md)


### Langages et Programmation

- Introduction [00_LangagesProg_intro.md](02_Module_Thematique/02_Langages/00_LangagesProg_intro.md)
- Activités diverses niveau 1re [01_LangagesProg_activite01.md](02_Module_Thematique/02_Langages/01_LangagesProg_activite01.md) basées sur la fiche élève [02_LangagesProg_FicheEleve01.pdf](02_Module_Thematique/02_Langages/02_LangagesProg_FicheEleve01.pdf) Les intentions de l'activité de découverte sont résumées ici : [02_LangagesProg_FicheProf01.md](02_Module_Thematique/02_Langages/02_LangagesProg_FicheProf01.md)


### Algorithmes

- Introduction [00_StructEtAlgo_intro.md](02_Module_Thematique/03_Algorithmes/00_StructEtAlgo_intro.md)
- Une activité sur les graphes [01_StructEtAlgo_activite01.md](02_Module_Thematique/03_Algorithmes/01_StructEtAlgo_activite01.md)


### Architectures

- Niveau SNT : présentation de TCP/IP

**Architecture matérielle et logicielle**

- Introduction [00_ArchiOS_intro.md](02_Module_Thematique/04_Architectures/00_ArchiOS_intro.md)

**Les réseaux**

- Introduction [03_reseaux_intro.md](02_Module_Thematique/04_Architectures/03_reseaux_intro.md)


## Module _Tranversal_

Ici on décline quelques sections par _façon d'aborder une ou plusieurs notions_ par exemple on pourrait avoir une section _Approche projet_ dans laquelle on peut avoir plusieurs façons de faire du projet : le mini projet de fin de matière pour approfondir plusieurs notions, le projet _fil rouge_ qui sert à introduire les notions et qui est donc déroulé tout au long de l'année etc.
Une section _Débranchée_ pour les nombreuses activités débranchées possibles.

### Modes Projets

### La communication autours de la spécialité NSI

### Les sujets de type BAC

### Les activités débranchées
